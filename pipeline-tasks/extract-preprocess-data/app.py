from flask import Flask, request
from flask_executor.executor import Executor
from genderizer import mainProcess
from os import pardir
from sys import exc_info
import pandas as pd
import json

api = Flask(__name__)
executor = Executor(api)
api.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True


@api.route('/processData/<request>', methods=['POST'])
def pre_process_data(request):
    '''
    Initiates a pre-processing thread
    '''
    if request is not None:
        try:
            executor.submit(mainProcess, request)
            # mainProcess(request)
            return "OK"
        except Exception as e:
            return type(e).__name__
    else:
        return "Not Allowed"


@api.route('/getData/<request>', methods=['GET'])
def get_data(request):
    '''
    push out the processed data
    '''
    try:
        data = pd.read_csv(f"{request}.csv", index_col=0)
        return json.dumps(data.to_json(orient='split'))
    except:
        return "Not Found"
