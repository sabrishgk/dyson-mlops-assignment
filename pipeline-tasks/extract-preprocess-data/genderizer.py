import stanza
import requests
import re
import pandas as pd
import random
import tqdm
import json
import numpy as np
from geopy.geocoders import Nominatim
import re

stanza.download()
nlp = stanza.Pipeline(lang='en', processors='tokenize,mwt,pos')


def mainProcess(request):
    data = pd.read_csv(
        "archive/AB_NYC_2019.csv",
        header=0,
        skiprows=lambda i: i > 0 and random.random() > 0.001)

    data = data.drop(['id', 'host_id'], axis=1)

    data = data[~data.host_name.isna() == True].reset_index().drop([
        'index'], axis=1)

    data["host_names"] = data.apply(lambda x: [name for name in re.findall(
        "[A-Z][a-z]+", x.host_name) if name not in ["And", "and", "AND", "or", "Or", "OR"]], axis=1)

    data = data.drop(['host_name'], axis=1)

    genders = []
    adjectives = []

    for val in tqdm.tqdm(data._values):
        genders.append(get_gender(val[13]))
        adjectives.append(count_adjectives(val[0]))

    data["genders"] = genders
    data["adjectives"] = adjectives

    data = data.drop(["host_names", "name"], axis=1)

    geolocator = Nominatim(user_agent="airbnb_price_predictor")

    location_points = {"statue of liberty": None, "central park": None, "times square": None,
                       "wall street": None, "empire state building": None, "metropolitan museum": None,
                       "nyu": None, "columbia university": None, "JFK": None, "LGA": None}

    for key in location_points.keys():
        location = geolocator.geocode(key)
        location_points[key] = (location.latitude, location.longitude)

    data["distances"] = data.apply(lambda x: [np.linalg.norm(np.array(
        (x.latitude, x.longitude)) - np.array(location_points[key])) for key in location_points.keys()], axis=1)
    data[["dSoL", "dCP", "dTS", "dWS", "dESB", "dMM", "dNYU", "dCU", "dJFK",
          "dLGA"]] = pd.DataFrame(data.distances.tolist(), index=data.index)
    data.drop(["distances"], axis=1, inplace=True)

    with open("archive/location_points.json", "r") as file:
        location_points = json.load(file)

    data["N_gr"] = data.apply(
        lambda x: location_points[x.neighbourhood], axis=1)
    data[["n_r_lat", "n_r_long"]] = pd.DataFrame(
        data.N_gr.tolist(), index=data.index)
    data.drop(["N_gr"], axis=1, inplace=True)

    data.drop(["neighbourhood", "latitude", "longitude"],
              axis=1, inplace=True)

    data.drop(['last_review'], axis=1, inplace=True)
    data.reviews_per_month.replace(np.nan, 0, inplace=True)

    data.to_csv(f"{request}.csv")


def get_gender(val):
    if val.__len__() > 2:
        return 'E'
    elif val.__len__() < 1:
        return 'N'
    else:
        for name in val:
            if name.__len__() > 15:
                return 'E'
            else:
                if nlp(name.lower()).get('upos').pop() == "PROPN":
                    name = re.sub('\D\W', '', name.lower())
                    if requests.get(f"http://app:5000/predict/{name}").text == 'Female':
                        return 'F'
                    else:
                        pass
                else:
                    return 'E'
        return 'NF'


def count_adjectives(text):
    """
    Ex: Big New Family Friendly Great Location -> 0.67
    """
    try:
        doc = nlp(text).get('upos')
        return doc.count('ADJ') / doc.__len__()
    except:
        return 0
