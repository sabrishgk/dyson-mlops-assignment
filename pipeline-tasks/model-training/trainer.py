from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import requests
import pandas as pd
import numpy as np
import time
import pickle
import json


def mainProcess(request):
    while True:
        r = requests.get(f"http://data:5001/getData/{request}")
        if r.text == 'Not Found':
            time.sleep(30)
        else:
            data = pd.read_json(json.loads(r.text), orient='split')
            print("data received !!!")
            break

    data.reviews_per_month.replace(np.nan, 0, inplace=True)
    X, y = data.drop(['price'], axis=1), data.price

    numerics = X.select_dtypes(include=np.number).columns.tolist()
    categoricals = [col for col in X.columns if col not in numerics]

    min_max_scl = MinMaxScaler()
    X_n = min_max_scl.fit_transform(X[numerics])

    one_hot_enc = OneHotEncoder()
    X_c = one_hot_enc.fit_transform(X[categoricals])

    X = pd.concat([pd.DataFrame(X_c.toarray(), columns=[val for cat in one_hot_enc.categories_
                                                        for val in cat.tolist()], dtype=int), pd.DataFrame(X_n, columns=numerics)], axis=1)

    log_y = pd.Series(np.log10(y)).replace(-np.inf, 0)

    X_train, X_test, y_train, y_test = train_test_split(
        X, log_y, test_size=0.1, random_state=42)

    rfr = RandomForestRegressor(random_state=42, n_jobs=8)
    rfr.fit(X_train, y_train)

    with open(f"{request}.pkl", "wb") as file:
        pickle.dump(rfr, file)

    print(
        f"MSE: {mean_squared_error(y_pred=rfr.predict(X_test), y_true=y_test)}")
