from copyreg import pickle
from flask import Flask, request
from flask_executor.executor import Executor
from trainer import mainProcess
from os import pardir
from sys import exc_info
import json
import pickle

api = Flask(__name__)
executor = Executor(api)
api.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True


@api.route('/train/<request>', methods=['POST'])
def train_model(request):
    '''
    Initiates a model training thread
    '''
    if request is not None:
        try:
            executor.submit(mainProcess, request)
            # mainProcess(request)
            return "OK"
        except Exception as e:
            return type(e).__name__
    else:
        return "Not Allowed"


@api.route('/getModel/<request>', methods=['GET'])
def get_model(request):
    '''
    push out the trained model
    '''
    try:
        with open(f"{request}.pkl", "rb") as file:
            obj = pickle.load(file)
        return json.dumps(obj, default=str)
    except:
        return "Not Found"
