```mermaid
graph TB

    subgraph dyson-mlops
        app((LSTM:5000))
        task1((data:5001<br/>:5001))
        task2((model:5002<br/>:5002))
        task1 -.GET:getData.-> task2
        app -.GET:predict/name.-> task1
        subgraph airflow
          op1(trigger_data_processing)-.XCOMS:request_id.->
          op2(trigger_model_training)
        end
    end

    task2 <--GET:getModel--> od3>Notebook/Local]
    op1 -.POST:processData<br/>:request_id.-> task1
    op2 -.POST:train<br/>:request_id.-> task2
    
     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:4px;
     class sq,e green
     class di orange
```