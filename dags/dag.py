from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
import requests


def trigger_data(**context):
    context["ti"].xcom_push(key="request_id", value="sample")
    r = requests.post("http://data:5001/processData/sample")
    print(r.status_code)


def trigger_model(**context):
    xcom = context["ti"].xcom_pull(key="request_id")
    r = requests.post(f"http://model:5002/train/{xcom}")


with DAG("assignment",
         start_date=days_ago(1),
         schedule_interval='@daily',
         catchup=False
         ) as dag:

    data = PythonOperator(
        task_id="trigger_data_processing",
        python_callable=trigger_data,
        provide_context=True
    )

    model = PythonOperator(
        task_id="trigger_model_training",
        python_callable=trigger_model,
        provide_context=True
    )

    data >> model
